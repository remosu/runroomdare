
var c = 0;
var requestId;
var height = 0;
var startTime = 0;
var totalTime = 10;
var score = 0;
var playing = false;

var colors = ['grey', 'blue', 'white', 'red', 'green'];
var figures = ['☁', '♟', '★', '☀', '♣'];

var cardIndex = 0;

var cards = [
    [[0, 0], [1, 2]],
    [[2, 0], [1, 1]],
    [[2, 2], [4, 1]],
    [[1, 0], [3, 3]],
    [[3, 2], [0, 1]],
    [[2, 0], [3, 4]],
    [[4, 2], [3, 0]],
    [[4, 3], [1, 0]],
    [[3, 2], [4, 1]],
    [[2, 0], [3, 1]],
    [[4, 2], [1, 3]],
    [[0, 3], [2, 1]],
    [[4, 3], [0, 1]],
    [[2, 3], [4, 1]],
    [[4, 0], [2, 3]],
    [[0, 2], [3, 1]],
    [[1, 2], [3, 4]],
    [[2, 3], [4, 0]],
    [[3, 4], [0, 1]],
    [[4, 0], [1, 2]],
    [[0, 4], [3, 2]],
    [[4, 3], [2, 0]],
    [[3, 4], [0, 1]],
    [[1, 2], [0, 3]],
    [[3, 2], [1, 4]],
    [[1, 2], [3, 0]]
];


function animate(time) {
    var dt = (time - startTime) / 1000 - score;
    var seconds = parseInt(dt, 10);
    var h = parseInt(dt / 10 * height, 10);
    $('#time_display').html(10-seconds);
    $('#level_bg').css('height', h+'px');
    requestId = window.requestAnimationFrame(animate);
    if ((totalTime - dt) <= 0) {
        playing = false;
        $('.start_dialog').slideDown('fast', function () {
            window.cancelAnimationFrame(requestId);
        });
    }
}

function restartScore() {
    score = 0;
    $('#score').html(score);
}

function start(time) {
    restartScore();
    startTime = time;
    requestId = window.requestAnimationFrame(animate);
    playing = true;
}

function setCard() {
    cardIndex = Math.floor(Math.random() * cards.length);
    var card = cards[cardIndex];
    $('#card0').html(figures[card[0][0]]);
    $('#card1').html(figures[card[1][0]]);
    $('#card0').css('color', colors[card[0][1]]);
    $('#card1').css('color', colors[card[1][1]]);
}

$('#start').on('click', function (eventsent) {
    event.preventDefault();
    setCard(0);
    $(this).parent().slideUp('fast');
    requestI = window.requestAnimationFrame(start);
});

$(window).on('load', function(event) {
    var i;
    var gameboardEl = $('#gameboard');
    FastClick.attach(document.body);
    gameboardEl.removeClass('hide');
    $(this).resize();
    for(i=0; i<5; i++) {
        setFigEvent(i);
    }
});

$(window).on('resize', function(event) {
    var gameboardEl = $('#gameboard');
    var windowRatio = $(window).width() / $(window).height();
    var boardRatio = gameboardEl.width() / gameboardEl.height();
    if ( windowRatio > boardRatio ) {
        gameboardEl.css('height', $(window).height());
        gameboardEl.css('width', $(window).height() * boardRatio + 'px');
    } else {
        gameboardEl.css('width', $(window).width());
        gameboardEl.css('height', $(window).width() / boardRatio + 'px');
    }

    $('.fig').css('line-height', gameboardEl.height() / 7 + 'px');
    $('.card_zone').css('line-height', gameboardEl.height() * 2 / 7 + 'px');

    gameboardEl.css('top', $(window).height() - gameboardEl.height());

    height = gameboardEl.height();
    height = 5 / 7 * height;
});

function setFigEvent(figIndex) {
    $('#fig'+figIndex).html(figures[figIndex]);
    $('#fig'+figIndex).on('click', function () {
        event.stopPropagation();
        event.preventDefault();
        if (playing && playOk(cardIndex, figIndex)) {
            score += 1;
            $('#score').html(score);
            setCard();
        }
    });
}

function playOk(cardIndex, figIndex) {
    var card = cards[cardIndex];
    if (_.uniq(_.flatten(card)).length === 3) {
        return figInCard(cardIndex, figIndex);
    }
    return figTotallyWrong(cardIndex, figIndex);
}

function figInCard(cardIndex, figIndex) {
    var card = cards[cardIndex];
    return _.any(card, function (fig) {
        return fig[0] === figIndex && fig[1] === figIndex;
    });
}

function figTotallyWrong(cardIndex, figIndex) {
    var card = cards[cardIndex];
    return !_.contains(_.flatten(card), figIndex);
}
